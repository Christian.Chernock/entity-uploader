## Auth

Make sure you are auth'd using:

`gcloud auth application-default login`

## Usage

1) Set agent ID

Set your agent id, by opening `entity-uploader.py`, and setting `agent_id = ""`. Agent ID can be found inside your agent, in the URL:

> dialogflow.cloud.google.com/cx/projects/gcp-cpe-1-l-secured-k895/locations/us-central1/agents/**7da520c1-4f57-41d3-81b2-d7c61c5fb342**/flows/...




2) Running program

- Create a new entity usage:

`./entity-uploader.py <entity name>`

- Update an entity usage:

`./entity-uploader.py <entity name> <entity ID>`

entity ID can be found when you click into an entity. It will be in the URL at the end:

`entityTypes?id=xxxxxxxx`
